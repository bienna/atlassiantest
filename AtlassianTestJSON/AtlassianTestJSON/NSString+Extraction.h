//
//  NSString+ReEx.h
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extraction)

- (NSArray *)arrayWithRegularExpression:(NSString *)pattern;
- (NSArray *)arrayOfUrls;

@end
