//
//  ViewController.m
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import "ViewController.h"
#import "JSONCreator.h"

NSString *const EXAMPLE_MESSAGE1 = @"@chris you around?";
NSString *const EXAMPLE_MESSAGE2 = @"Good morning! (megusta) (coffee) (bbbbbbbbbbbbbbb) (aaaaaaaaaaaaaaaaaaaaaaaaaaaa)";
NSString *const EXAMPLE_MESSAGE3 = @"@bob @john (success) Olympics are starting soon; http://www.nbcolympics.com";
NSString *const EXAMPLE_MESSAGE4 = @"@chris Good morning! (megusta) (coffee) http://vnexpress.net/ https://www.tinhte.vn/ https://www.facebook.com/ https://twitter.com/jdorfman/status/430511497475670016";

@interface ViewController ()

@property (nonatomic, weak) IBOutlet UITextField *textFieldInput;
@property (nonatomic, weak) IBOutlet UIButton *buttonGenerate;
@property (nonatomic, weak) IBOutlet UITextView *textViewResult;

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (IBAction)onClickButtonGenerate:(id)sender {
	[_textFieldInput resignFirstResponder];
	NSString *text = _textFieldInput.text;
	__weak typeof (self) weakSelf = self;
	[JSONCreatorShared createJSONStringWithMessage:text completion:^(id data, NSError *error) {
		NSLog(@"%@", data);
		if (!error) {
			if (data) {
				weakSelf.textViewResult.text = data;
			} else {
				weakSelf.textViewResult.text = @"";
			}
		} else {
			weakSelf.textViewResult.text = @"Cannot parse this message";
		}
	}];
}

- (IBAction)onClickButtonExample1:(id)sender {
	_textFieldInput.text = EXAMPLE_MESSAGE1;
	_textViewResult.text = @"";
}

- (IBAction)onClickButtonExample2:(id)sender {
	_textFieldInput.text = EXAMPLE_MESSAGE2;
	_textViewResult.text = @"";
}

- (IBAction)onClickButtonExample3:(id)sender {
	_textFieldInput.text = EXAMPLE_MESSAGE3;
	_textViewResult.text = @"";
}

- (IBAction)onClickButtonExample4:(id)sender {
	_textFieldInput.text = EXAMPLE_MESSAGE4;
	_textViewResult.text = @"";
}

@end
