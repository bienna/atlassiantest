//
//  NetworkActivityIndicator.m
//
//
//  Created by BienNA on 7/18/14.
//  Copyright (c) 2014 BienNA. All rights reserved.
//

#import "NetworkActivityIndicator.h"

@implementation NetworkActivityIndicator

static NetworkActivityIndicator *instance;

- (void) visible{
	@synchronized ([NetworkActivityIndicator class]) {
		if (!instance) {
			instance = self;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
		}
	}
}

- (void) disable{
	@synchronized ([NetworkActivityIndicator class]) {
		if (self == instance) {
			instance = nil;
			[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
		}
	}
}

@end
