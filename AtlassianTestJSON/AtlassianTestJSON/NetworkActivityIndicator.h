//
//  NetworkActivityIndicator.h
//  
//
//  Created by BienNA on 7/18/14.
//  Copyright (c) 2014 BienNA. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface NetworkActivityIndicator : NSObject

- (void) visible;

- (void) disable;

@end
