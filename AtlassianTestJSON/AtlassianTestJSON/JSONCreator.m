//
//  JSONCreator.m
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import "JSONCreator.h"
#import "NSString+Extraction.h"
#import "NetworkActivityIndicator.h"

NSString *const REGULAR_EXPRESSION_MENTION = @"(?<=@)\\w+";
NSString *const REGULAR_EXPRESSION_EMOTICON = @"(?<=\\()([\\w]{1,15})(?=\\))";
NSString *const REGULAR_EXPRESSION_TITLE = @"(?<=<title>)(.*)(?=</title>)";

@interface JSONCreator()

@property (nonatomic, strong) NetworkActivityIndicator *networkActivityIndicator;

@end

@implementation JSONCreator

+ (id)sharedCreator {
	static JSONCreator *sharedMyManager = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		sharedMyManager = [[self alloc] init];
	});
	return sharedMyManager;
}

- (id)init {
	if (self = [super init]) {
		self.networkActivityIndicator = [NetworkActivityIndicator new];
		return self;
	}
	return nil;
}

- (void)createJSONStringWithMessage:(NSString *)message completion:(CompletionBlock)completion {
	[_networkActivityIndicator visible];
	NSArray *mentions = [message arrayWithRegularExpression:REGULAR_EXPRESSION_MENTION];
	NSArray *emoticons = [message arrayWithRegularExpression:REGULAR_EXPRESSION_EMOTICON];
	
	NSArray *urls = [message arrayOfUrls];
	if (urls.count > 0) {
		__weak typeof (self) weakSelf = self;
		
		NSMutableArray *links = [NSMutableArray array];
		[urls enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			HttpRequest *request = [HttpRequest new];
			[request request:obj completion:^(id data, NSError *error) {
				NSString *title = @"";
				if (!error) {
					if (data) {
						NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
						NSRange range = [responseString rangeOfString:REGULAR_EXPRESSION_TITLE options:NSRegularExpressionSearch];
						if (range.location != NSNotFound) {
							title = [responseString substringWithRange:range];
						}
					}
				} else {
					title = @"Cannot get title";
				}
				
				[links addObject:@{@"url" : obj,
								   @"title" : title}];
				if (links.count == urls.count) {
					[self createJSONWithMention:mentions emoticon:emoticons link:links completion:completion];
					[weakSelf.networkActivityIndicator disable];
				}
			}];
		}];
	} else {
		[self createJSONWithMention:mentions emoticon:emoticons link:@[] completion:completion];
		[_networkActivityIndicator disable];
	}
}

- (void)createJSONWithMention:(NSArray *)mentions emoticon:(NSArray *)emoticons link:(NSArray *)links completion:(CompletionBlock)completion {
	NSDictionary *jsonDict = @{@"mentions" : mentions,
							   @"emoticons" : emoticons,
							   @"links" : links};
	NSError * error = nil;
	NSData * jsonData = [NSJSONSerialization dataWithJSONObject:jsonDict options:NSJSONWritingPrettyPrinted error:&error];
	//	id json = [NSJSONSerialization JSONObjectWithData:jsonData
	//											  options:NSJSONReadingMutableLeaves
	//												error:&error];
	if (!error) {
		if (completion) {
			completion([[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding], nil);
		}
	} else {
		if (completion) {
			completion(nil, error);
		}
	}
}

@end
