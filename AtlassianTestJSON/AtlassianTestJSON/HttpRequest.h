//
//  HttpRequest.h
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^CompletionBlock)(id data, NSError *error);

@interface HttpRequest : NSObject {
	CompletionBlock _completion;
}

- (void)request:(NSString *)url completion:(CompletionBlock)completion;

@end
