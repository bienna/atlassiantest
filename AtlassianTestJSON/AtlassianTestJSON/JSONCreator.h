//
//  JSONCreator.h
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HttpRequest.h"

#define JSONCreatorShared ((JSONCreator *)[JSONCreator sharedCreator])

@interface JSONCreator : NSObject

@property (nonatomic, copy) NSString *plainString;

+ (id)sharedCreator;
- (void)createJSONStringWithMessage:(NSString *)message completion:(CompletionBlock)completion;

@end
