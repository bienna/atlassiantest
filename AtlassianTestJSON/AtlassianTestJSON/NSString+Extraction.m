//
//  NSString+ReEx.m
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import "NSString+Extraction.h"

@implementation NSString (Extraction)

- (NSArray *)arrayWithRegularExpression:(NSString *)pattern {
	__weak typeof (self) weakSelf = self;
	NSMutableArray *matchStringArray = [NSMutableArray new];
	
	NSRange range = NSMakeRange(0, self.length);
	NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
	NSArray *matches = [regex matchesInString:self options:NSMatchingReportProgress range:range];
	[matches enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if ([obj isKindOfClass:[NSTextCheckingResult class]]) {
			NSTextCheckingResult *match = (NSTextCheckingResult *)obj;
			NSRange matchRange = match.range;
			[matchStringArray addObject:[weakSelf substringWithRange:matchRange]];
		}
	}];
	
	return matchStringArray;
}

- (NSArray *)arrayOfUrls {
	__weak typeof (self) weakSelf = self;
	NSMutableArray *urlArray = [NSMutableArray new];
	
	NSDataDetector* detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
	NSArray* matches = [detector matchesInString:self options:0 range:NSMakeRange(0, self.length)];
	[matches enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if ([obj isKindOfClass:[NSTextCheckingResult class]]) {
			NSTextCheckingResult *match = (NSTextCheckingResult *)obj;
			NSRange matchRange = match.range;
			[urlArray addObject:[weakSelf substringWithRange:matchRange]];
		}
	}];
	
	return urlArray;
}

@end
