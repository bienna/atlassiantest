//
//  HttpRequest.m
//  AtlassianTestJSON
//
//  Created by COD-iMacNo1 on 5/22/15.
//  Copyright (c) 2015 BienNA. All rights reserved.
//

#import "HttpRequest.h"

@interface HttpRequest() <NSURLConnectionDelegate>

@property (nonatomic) NSURLConnection *connection;
@property (nonatomic, assign) BOOL isCompeletion;

@end

@implementation HttpRequest

- (id)init {
	if (self = [super init]) {
		self.isCompeletion = NO;
		return self;
	}
	return nil;
}

- (void)request:(NSString *)url completion:(CompletionBlock)completion {
	_completion = completion;
	NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]
														   cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData
													   timeoutInterval:30];
	[request setHTTPShouldHandleCookies:YES];
	self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
	return [protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust];
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
	if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
		[challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
	}
	[challenge.sender continueWithoutCredentialForAuthenticationChallenge:challenge];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
	
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
	if (!_isCompeletion) {
		if (_completion) {
			_completion(data, nil);
		}
		self.isCompeletion = YES;
	}
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
	if (!_isCompeletion) {
		if (_completion) {
			_completion(nil, error);
		}
		self.isCompeletion = YES;
	}
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
	
}

@end
